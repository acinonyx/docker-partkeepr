# PartKeepr Docker image
#
# Copyright (C) 2022 Libre Space Foundation <https://libre.space/>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

ARG PHP_IMAGE_TAG=7.1.33-fpm-alpine3.10
FROM php:${PHP_IMAGE_TAG}
LABEL org.opencontainers.image.authors='LSF operations team <ops@libre.space>'

ARG PARTKEEPR_DIR=/opt/partkeepr
ARG PARTKEEPR_VERSION=1.4.0
ARG PHP_APCU_BC_VERSION=1.0.5

# Use production configuration
RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"

RUN apk add --no-cache \
	rsync \
	freetype \
	icu \
	libjpeg \
	libpng \
	libwebp \
	libxml2 \
	libldap \
	libpq \
	postgresql-client \
	mysql-client

# Get PartKeepr source code
RUN cd /tmp \
	&& curl -O https://downloads.partkeepr.org/partkeepr-${PARTKEEPR_VERSION}.tbz2 \
	&& mkdir -p ${PARTKEEPR_DIR} \
	&& tar -C ${PARTKEEPR_DIR} -xjf /tmp/partkeepr-${PARTKEEPR_VERSION}.tbz2 --strip-components=1 \
	&& rm /tmp/partkeepr-${PARTKEEPR_VERSION}.tbz2

# Set Partkeepr cron job
RUN echo "0 0,6,12,18 * * * /usr/local/bin/php /var/www/html/app/console partkeepr:cron:run" > /etc/crontabs/www-data

# Install PHP extensions
RUN apk add --no-cache --virtual .build-deps \
		${PHPIZE_DEPS} \
		freetype-dev \
		icu-dev \
		jpeg-dev \
		libpng-dev \
		libwebp-dev \
		libxml2-dev \
		openldap-dev \
		postgresql-dev \
		postgresql-client \
		mysql-client \
	&& pecl channel-update pecl.php.net \
	&& pecl install \
		apcu_bc-${PHP_APCU_BC_VERSION}  < /dev/null \
	&& docker-php-ext-configure \
		gd \
			--with-jpeg-dir=/usr/include \
			--with-png-dir=/usr/include \
			--with-webp-dir=/usr/include \
			--with-freetype-dir=/usr/include \
			--enable-gd-native-ttf \
	&& docker-php-ext-install -j "$(nproc)" \
		bcmath \
		ldap \
		gd \
		dom \
		opcache \
		intl \
		pdo_mysql \
		pdo_pgsql \
	&& docker-php-ext-enable --ini-name 00-apc.ini apcu apc \
	&& pecl clear-cache \
	&& apk del --no-network .build-deps

# Create directories and volumes
VOLUME "${PARTKEEPR_DIR}"

# Add container entrypoint
COPY docker-entrypoint.sh /
ENTRYPOINT ["/docker-entrypoint.sh"]

CMD ["partkeepr"]
